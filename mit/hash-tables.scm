#| MIT Scheme Hash-tables

Copyright (C) 2020 Aidan Nyquist
Copyright (C) 2016 Federico Beffa

This file is part of the MIT Scheme compatibility library.

This library is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.

|#

;;; Code:

(library (mit hash-tables)
  (export make-strong-eq-hash-table
          make-key-weak-eq-hash-table
	  make-weak-eq-hash-table
	  make-eq-hash-table
          make-strong-eqv-hash-table
	  make-key-weak-eqv-hash-table
	  hash-table/get
	  hash-table/put!
	  eqv-hash-mod
	  equal-hash-mod
	  equal-hash
	  eqv-hash
	  weak-hash-table/constructor
	  strong-hash-table/constructor
	  hash-table/intern!
	  hash-table/key-list
	  hash
	  ;; TODO:
	  ;; Re-export from srfi-69
	  alist->hash-table
	  hash-table->alist)
  (import (mit core)
	  (ice-9 curried-definitions)
	  (rnrs arithmetic fixnums)
	  (only (guile)
		eq? eqv?
                hashq hashv
		remainder
		and
		not
		lambda*
		if
		quote
		number?
		let
		unless
		gensym)
	  (rename (srfi :69)
		  (hash srfi-hash)))


(define default-hash-size 50)

(define (hash-size size)
  (if size
      size
      default-hash-size))

(define* (make-strong-eq-hash-table #:optional size)
  (make-hash-table eq? #:weak #f (hash-size size)))

(define* (make-key-weak-eq-hash-table #:optional size)
  (make-hash-table eq? #:weak #f (hash-size size)))

(define* (make-weak-eq-hash-table #:optional size)
  (make-hash-table eq? #:weak #f (hash-size size)))

(define* (make-strong-eqv-hash-table #:optional size)
  (make-hash-table eqv? #:weak #f (hash-size size)))

(define* (make-key-weak-eqv-hash-table #:optional size)
  (make-hash-table eqv? #:weak #f (hash-size size)))

(define* (make-weak-eqv-hash-table #:optional size)
  (make-hash-table eqv? #:weak #f (hash-size size)))

(define make-eq-hash-table make-strong-eq-hash-table)

(define hash-table/get hash-table-ref/default)

(define hash-table/put! hash-table-set!)

(define (hash-table/intern! table key get-default)
  (let ((default (get-default)))
    (unless (hash-table-exists? table key)
      (hash-table/put! table key default))
    (hash-table/get table key default)))

(define (eqv-generic? x)
  ;; all numbers except fixnums must go through generic hashtable
  (and (number? x)
       (not (fixnum? x))))

(define eq-hash hash-by-identity)

(define eqv-hash hashv)

(define equal-hash srfi-hash)

;; (if (eqv-generic? key)
;;     ;; equal-hash passes numbers to number-hash, as the internal eqv-hash
;;     (equal-hash key)
;;     (symbol-hash key))

(define* (eqv-hash-mod key modulus)
   (eqv-hash key modulus))

(define* (equal-hash-mod key modulus)
   (equal-hash key modulus))

(define* (hash key #:optional modulus)
  (if (default-object? modulus)
      (equal-hash key)
      (equal-hash-mod key modulus)))

;; TODO: We now have weak hashtables
;; XXX: since we do not have a generic weak hashtable constructor,
;; we make weak-pairs in generic/weak.scm normal pairs.
(define* (weak-hash-table/constructor key-hash key-equal #:optional rehash-after-gc)
  (hash-table-construct key-hash key-equal #f))

(define* (strong-hash-table/constructor key-hash key-equal #:optional rehash-after-gc)
  (hash-table-construct key-hash key-equal #f))

(define (hash-table-construct key-hash key-equal weak)
  (lambda* (#:optional size)
    (make-hash-table key-equal key-hash #:weak weak (hash-size size))))

(define hash-table/key-list hash-table-keys)
)

