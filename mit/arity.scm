#| MIT Scheme Arity

Copyright (C) 2020 Aidan Nyquist
Copyright (C) 2016 Federico Beffa

This file is part of the MIT Scheme compatibility library.

This library is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.

|#

;;; Code:

(library (mit arity)
  (export procedure-name procedure-arity procedure-arity?
          make-procedure-arity procedure-arity-min procedure-arity-max
          procedure-arity-valid? procedure-of-arity?
          guarantee-procedure-of-arity
          guarantee-procedure guarantee-procedure-arity
          thunk? guarantee-thunk)
  (import (rnrs)
          (ice-9 format)
          (ice-9 curried-definitions)
          (only (guile)
		procedure-name
		procedure-minimum-arity
		object->string))

;;; Guarantors

(define-syntax define-guarantor
  (syntax-rules ()
    ((_ guarantor predicate)
     (define (guarantor obj . ctx)
       (if (predicate obj)
           obj
           (error 'guarantor
                  (format #f "Wrong type argument in context ~a" ctx)
                  obj))))))

(define-guarantor guarantee-procedure procedure?)
(define-guarantor guarantee-index-fixnum index-fixnum?)
(define-guarantor guarantee-thunk thunk?)
(define-guarantor guarantee-procedure-arity procedure-arity?)

;; user defined procedures

(define make-procedure-arity
  (case-lambda
    ((a-min) (make-procedure-arity a-min a-min #f))
    ((a-min a-max) (make-procedure-arity a-min a-max #f))
    ((a-min a-max simple-ok?)
     (guarantee-index-fixnum a-min 'make-procedure-arity)
     (unless (or (index-fixnum? a-max) (eq? a-max #f))
       (error 'make-procedure-arity "Wrong type max arity: " a-max))
     (if (and simple-ok? (= a-min a-max))
	 a-min
	 (cons a-min a-max)))))

(define (parameter-list->arity arglist)
  (let loop ((arglist arglist)
             (optionals? #f)
             (required '())
             (optional '()))
    (cond ((null? arglist)
           (let ((arg-min (length required)))
             (make-procedure-arity arg-min (+ arg-min (length optional)))))
          ((symbol? arglist)
           (make-procedure-arity (length required) #f))
          (else
           (loop (cdr arglist) optionals?
                 (if optionals? required (cons (car arglist) required))
                 (if optionals? (cons (car arglist) optional) optional))))))

(define (%arities-union a1 a2)
  (let ((a1-min (procedure-arity-min a1))
        (a1-max (procedure-arity-max a1))
        (a2-min (procedure-arity-min a2))
        (a2-max (procedure-arity-max a2)))
    (let ((a-max (if (or (not a1-max) (not a2-max)) #f (max a1-max a2-max))))
      (make-procedure-arity (min a1-min a2-min) a-max))))

;; XXX: this is of course an approximation, but backed by common-sense.
(define (parameter-lists->union-arity arglists)
  (let ((arities (map parameter-list->arity arglists)))
    (fold-left %arities-union (car arities) (cdr arities))))

;; returns (min . max/#f) MIT/Scheme style.
(define (procedure-arity proc)
  (let ((arity (procedure-minimum-arity proc)))
    ; convert from guile arity: (min opt more-possible)
    ; to mit arity: (min . max) max is #f if more-possible
    (cons (car arity)
	  (and (not (caddr arity))
	       (+ (car arity) (cadr arity))))))


(define index-fixnum? fixnum?)

(define (simple-arity? object)
  (index-fixnum? object))

(define (general-arity? object)
  (and (pair? object)
       (index-fixnum? (car object))
       (if (cdr object)
           (and (index-fixnum? (cdr object))
                (fx>=? (cdr object) (car object)))
           #t)))

(define (procedure-arity? object)
  (if (simple-arity? object)
      #t
      (general-arity? object)))

(define (procedure-arity-max arity)
  (cond ((simple-arity? arity) arity)
        ((general-arity? arity) (cdr arity))
        (else (error 'procedure-arity-max
                     "Argument isn't a procedure arity" arity))))

(define (procedure-arity-min arity)
  (cond ((simple-arity? arity) arity)
        ((general-arity? arity) (car arity))
        (else (error 'procedure-arity-min
                     "Argument isn't a procedure arity" arity))))

(define (procedure-arity-valid? procedure n-arguments)
  (guarantee-index-fixnum n-arguments 'PROCEDURE-ARITY-VALID?)
  (let ((arity (procedure-arity procedure)))
    (and (<= (car arity) n-arguments)
         (or (not (cdr arity))
             (<= n-arguments (cdr arity))))))

(define (procedure-of-arity? proc arity)
  (and (procedure? proc) (procedure-arity-valid? proc arity)))

(define (guarantee-procedure-of-arity proc arity msg)
  (if (not (and (<= (car (procedure-arity proc))
		    arity)
		(or (not (cdr (procedure-arity proc)))
		    (>= (car (procedure-arity proc))
			arity))))
      (error msg proc "not of arity" arity)))

(define (thunk? proc) (procedure-of-arity? proc 0))

)



