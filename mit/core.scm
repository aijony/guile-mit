#| Core MIT Scheme Compatibility

Copyright (C) 2020 Aidan Nyquist
Copyright (C) 2016 Federico Beffa

This file is part of the MIT Scheme compatibility library.

This library is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.

|#

;;; Code:

(library (mit core)
  (export declare usual-integrations integrate-operator integrate
	  assert ignore-errors bkpt
	  guarantee-symbol
	  exact-rational? exact-positive-integer? exact-integer?
	  exact-nonnegative-integer?
	  guarantee-exact-integer
	  guarantee-exact-positive-integer guarantee-exact-nonnegative-integer
	  start-canonicalizing-symbols!
	  start-preserving-case!
	  default-object?
	  pp print
	  define-integrable
	  true false unspecific
	  runtime
	  there-exists? for-all?
	  symbol<? generate-uninterned-symbol string->uninterned-symbol
	  undefined-value?
	  string-find-next-char-in-set string-search-forward string-head
	  string-tail
	  write-line
	  symbol-append
	  symbol
	  delq delv
	  run-shell-command user-homedir-pathname system-tmpdir-pathname
	  ->namestring
	  fluid-let
	  graphics-type graphics-type-name)
  (import (rename (except (rnrs) error assert) (remq delq) (remv delv))
	  (ice-9 pretty-print)
	  (ice-9 curried-definitions)
	  (prefix (only (rnrs) assert) r6rs:)
	  (only (srfi :19)
		current-time
		time-second
		time-nanosecond)
	  (only (system vm trap-state)
		add-trap-at-procedure-call!)
	  (only (guile) make-fluid fluid-set!
		fluid-ref gensym getenv
		system error eval
                *unspecified*
		read-disable read-enable)
	  (only (srfi :13) string-index string-contains)
	  (only (srfi :1) every any)
	  (except (mit arity) procedure-arity) ; use version in apply-hook
	  (mit arithmetic))


;; Declare is used at the start of files.  According to R6RS a library
;; must start with definitions.  (define (declare args) (if #f #f))
(define-syntax declare
  (syntax-rules ()
    ((_ args ...) (define (args ...) #f))))

;; (define (declare . args) #f)

(define (usual-integrations . args) #f)

(define-syntax integrate-operator
  (syntax-rules ()
    ((_ args ...) unspecific)))

(define-syntax integrate
  (syntax-rules ()
    ((_ args ...) unspecific)))

(define (start-canonicalizing-symbols!)
  (read-disable 'case-insensitive))

(define (start-preserving-case!)
  (read-enable 'case-insensitive))


(define (default-object? x)
  (eq? #f x))

(define-syntax define-integrable
  (syntax-rules ()
    ((_ form body ...) (define form body ...))))

(define print pretty-print)

(define* (pp object #:optional (output-port (current-output-port)) (as-code #f))
  (pretty-print object output-port))

(define-syntax assert
  (syntax-rules ()
    ((_ form rest ...) (r6rs:assert form))))

(define* (ignore-errors thunk #:optional map-error)
  (call-with-current-continuation
   (lambda (k)
     (with-exception-handler
	 (lambda (x)
	   (cond ((or (default-object? map-error)
		      (not map-error))
		  (if (error? x) (k x) x))
		 ((and (procedure? map-error)
		       (procedure-arity-valid? map-error 1))
		  (lambda (condition)
		    (k (map-error condition))))
		 (else
		  (error "wrong-type-argument" map-error
			 "map-error procedure"
			 'IGNORE-ERRORS))))
       thunk))))


(define (bkpt datum . arguments)
  (let ()
    (define (broke datum . arguments)
      (display "breakpoint"))
    (add-trap-at-procedure-call! broke)
    (broke datum arguments)))

(define (guarantee-symbol x msg)
  (if (not (symbol? x))
      (error msg x "not a symbol")))

(define (exact-rational? x)
  (and (rational? x)
       (exact? x)))

(define (exact-positive-integer? x)
  (and (integer? x)
       (positive? x)
       (exact? x)))

(define (exact-integer? x)
  (and (integer? x)
       (exact? x)))

(define (exact-nonnegative-integer? x)
  (and (integer? x)
       (not (negative? x))
       (exact? x)))

(define (guarantee-exact-integer x msg)
  (if (not (exact-integer? x))
      (error msg x "not an exact integer")))

(define (guarantee-exact-positive-integer x msg)
  (if (not (exact-positive-integer? x))
      (error msg x "not an exact positive integer")))

(define (guarantee-exact-nonnegative-integer x msg)
  (unless (or (zero? x) (exact-positive-integer? x))
    (error msg x "not an exact positive integer")))

(define true #t)
(define false #f)

(define unspecific *unspecified*)

(define (runtime)
  (let ((t (current-time 'time-process)))
    (+ (time-second t)
       (/ (time-nanosecond t) 1e9))))

(define (there-exists? items predicate)
  (let loop ((items* items))
    (if (pair? items*)
        (if (predicate (car items*))
            #t
            (loop (cdr items*)))
        (begin
          (if (not (null? items*))
              (error ":not-list items" 'THERE-EXISTS?))
          #f))))

(define (for-all? p l) (every l p))

(define (symbol<? x y)
  (string<? (symbol->string x)
            (symbol->string x)))

(define* (generate-uninterned-symbol #:optional s)
  (if s
      (if (symbol? s) 
	  (gensym (symbol->string s))
	  (gensym s))
      (gensym)))

(define* (string->uninterned-symbol #:optional a)
  (gensym a))

(define (undefined-value? object)
  (or (eq? object unspecific)
      ;;(and (variable? object) (not (variable-bound? object)))
      ;;(eq? object (object-new-type (ucode-type constant) 2))
      ))

(define string-find-next-char-in-set string-index)

(define (string-search-forward pattern string)
  (string-contains string pattern))

(define (string-head string end)
  (substring string 0 end))

(define (string-tail string start)
  (substring string start (string-length string)))

(define write-line
  (case-lambda
    ((obj) (write-line obj (current-output-port)))
    ((obj port) (display obj port) (newline port))))

(define symbol-append
  (lambda args
    (string->symbol (apply string-append (map symbol->string args)))))

(define (symbol . args)
  (define (ensure-symbol s)
    (cond
     ((symbol? s) s)
     ((number? s) (string->symbol (number->string s)))
     ((string? s) (string->symbol s))
     (else (error "wrong type" s 'symbol))))
  (apply symbol-append (map ensure-symbol args)))

(define (run-shell-command cmd . rest)
  (system cmd))

(define (user-homedir-pathname)
  (string-append (getenv "HOME") "/"))

(define (system-tmpdir-pathname) "/tmp/")

(define (->namestring pathname)
  pathname)

;;; temporary graphics fix
(define (graphics-type arg) #f)

;; This function is used in a context where symbols are canonicalized.
;; Therefore we emit a lowercase symbol.  (This module is loaded
;; preserving case.)
(define (graphics-type-name name) 'x)

(define-syntax fluid-let
  (syntax-rules ()
    ((fluid-let ((var1 val1)
		 (var2 val2)
		 ...)
       body1
       body2
       ...)
     (let ((var1 (make-fluid))
	   (var2 (make-fluid))
	   ...)
       (fluid-set! var1 val1)
       (fluid-set! var2 val2)
       ...
       (let ((var1 (fluid-ref var1))
	     (var2 (fluid-ref var2))
	     ...)
	 body1
	 body2
	 ...)))))
)
