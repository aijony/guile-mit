#| MIT Scheme Environments

Copyright (C) 2020 Aidan Nyquist
Copyright (C) 2016 Federico Beffa

This file is part of the MIT Scheme compatibility library.

This library is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.

|#

;;; Code:

(library (mit environment)
  (export ge access
          user-initial-environment
          system-global-environment
          environment-define
	  environment?
          extend-top-level-environment
          environment-bindings
          nearest-repl/environment
          environment-bound?
          environment-assign!
          environment-lookup
          environment-link-name
          environment-update-from-child)
  (import (except (rnrs) error assert)
	  (only (guile)
		interaction-environment
		resolve-module
		module-define!
		make-module
		module?
		module-use!
		module-for-each
		module-defined?
		module-set!
		module-variable
		module-ref)
          (only (ice-9 r5rs)
		scheme-report-environment))

(define (copy-environment env)
  env)

(define (scheme-environment)
  (scheme-report-environment 5))

(define user-initial-environment
  ;;(interaction-environment)
  (copy-environment (scheme-environment)))

(define user-generic-environment
  (interaction-environment))

(define system-global-environment
  (copy-environment (scheme-environment)))

(define (ge env)
  (interaction-environment)) ;;env))

;; XXX: should add the set! use. (identifier-syntax?)
(define-syntax access
  (syntax-rules ()
    ((_ name env) (module-variable env 'name))))

(define (environment-define env var obj)
  (module-define! env var obj))

(define environment? module?)

(define (extend-top-level-environment top-env)
  (let ((new-environment (make-module)))
    (module-use! new-environment top-env)
    new-environment))

(define (environment-bindings module)
  (if module
      (let ((output '()))
	(module-for-each
	 (lambda (k v)
	   (set! output (cons `(,k ,v)
			      output))) module)
	output)
      '()))

(define nearest-repl/environment
  (lambda () (interaction-environment)))

(define (environment-bound? env symbol)
  (module-defined? env symbol))

(define (environment-assign! env symbol object)
  (module-set! env symbol object))

(define (environment-lookup env symbol)
  (module-ref env symbol))

(define (environment-link-name dest-env src-env name)
  (module-define! dest-env name (module-ref src-env name)))

(define environment-update-from-child
  (case-lambda
    ((env child-env)
     (environment-update-from-child env child-env (environment-bindings child-env)))
    ((env child-env child-symbols)
     (for-each (lambda (s)
		 (unless (module-defined? env s)
		   (module-define! env s (module-variable child-env s))))
	       child-symbols))))

)

